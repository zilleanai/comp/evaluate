import os
import gc
import json
from io import StringIO, BytesIO
import pickle
import multiprocessing

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from zworkflow import Config
from zworkflow.dataset import get_dataset
from zworkflow.preprocessing import get_preprocessing
from zworkflow.model import get_model
from zworkflow.evaluate import get_evaluate


@celery.task(serializer='pickle')
def evaluate_async_task(project, id, data):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
    configfile = os.path.join('workflow.yml') or {}
    config = Config(configfile)
    config['general']['verbose'] = False

    model = get_model(config)
    preprocessing = get_preprocessing(config)
    dataset = get_dataset(config, preprocessing, data)
    evaluate = get_evaluate(config)
    result = evaluate.evaluate(dataset, model)
    result['id'] = id
    redis = AppConfig.SESSION_REDIS
    if not data is None:
        redis.setex(''+id+'_orig', 1800, data)
    for image in result.get('images'):
        with open(image, "rb") as f:
            data = f.read()
            redis.setex(''+id+'_'+image, 1800, data)
    redis.setex(id, 1800, json.dumps(result))
