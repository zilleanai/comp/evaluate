# evaluate

Component for evaluate in workflow.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/evaluate.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.evaluate',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.evaluate.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Evaluate,
} from 'comps/evaluate/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Evaluate: 'Evaluate',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Evaluate,
    path: '/evaluate',
    component: Evaluate,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Evaluate} />
    ...
</div>
```
