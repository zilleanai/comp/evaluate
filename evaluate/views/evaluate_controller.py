import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

from werkzeug.utils import secure_filename
from ..tasks import evaluate_async_task


class EvaluateController(Controller):

    def evaluate_task(self, project, data):
        id = str(uuid.uuid4())+''
        if data:
            data = data.read()
        evaluate_async_task.delay(project, id, data)
        return id

    @route('/download/<string:id>')
    def download(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        return cached

    @route('/evaluation/<string:id>')
    def evaluation(self, id):
        return jsonify(json.loads(self.download(id)))

    @route('/evaluate/<string:project>/file', methods=['POST'])
    def evaluate_file(self, project):
        if 'filepond' not in request.files:
            return abort(404)
        file = request.files['filepond']
        print(file)
        if file.filename == '':
            return abort(404)
        id = self.evaluate_task(project, file.stream)
        return jsonify({'id': id, 'project': project, 'filename': file.filename})

    @route('/evaluate/<string:project>')
    def evaluate(self, project):
        id = self.evaluate_task(project, None)
        return jsonify({'id': id, 'project': project})
