import React from 'react'
import { compose } from 'redux'
import './run-evaluate.scss'
import { v1 } from 'api'
import { get} from 'utils/request'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'
import { ResultData } from 'comps/evaluate/components'

registerPlugin(FilePondPluginImagePreview);


function evaluate(uri) {
  return v1(`/evaluate${uri}`)
}

class RunEvaluate extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: [],
      results: [],
      resultids: []
    };
  }

  handleInit() {
  }

  addResult = (res) =>{
    const result = {
      id: res['id'],
    }
    const results = this.state.results
    const resultids = this.state.resultids
    results.push(result)
    resultids.push(result.id)
    this.setState({ results: results, resultids: resultids })
  }

  render() {
    const { results, resultids } = this.state
    const server = {
      url: evaluate(`/evaluate/${storage.getProject()}/file`),
      timeout: 160000,
      process: {
        onload: (res) => {
          const res_obj = JSON.parse(res)
          this.addResult(res_obj)
          return res;
        }
      }
    }

    return (
      <div className="RunEvaluate">

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={server}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state

            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}


        </FilePond>
        <button type='button' onClick={()=>{
          get(v1(`/evaluate/evaluate/${storage.getProject()}`)).then((res)=>{
            this.addResult(res)
          })
        }} >Evaluate</button>
        <label>results:</label>
        <ul className="results">
          {results.map((result, i) => {
            return (
              <li key={i} >
                <ResultData id={result['id']} />
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default compose(
)(RunEvaluate)
