import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { InfoBox, PageContent } from 'components'
import { RunEvaluate } from 'comps/evaluate/components'

class Evaluate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { isLoaded} = this.props
    return (
      <PageContent>
        <Helmet>
          <title>Evaluate</title>
        </Helmet>
        <h1>Evaluate!</h1>
        <RunEvaluate />
      </PageContent>)
  }
}

export default compose(
)(Evaluate)
