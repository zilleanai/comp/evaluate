import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { List } from 'immutable';
import './result-data.scss'
import { CheckboxList } from 'components'
import { v1 } from 'api'
import { loadEvalData } from 'comps/evaluate/actions'
import { selectEvalDataById } from 'comps/evaluate/reducers/evalData'

class ResultData extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      reloadStr: ''
    }
  }

  componentWillMount() {
    const { loadEvalData, id } = this.props
    loadEvalData.maybeTrigger({ id })
  }

  componentDidMount() {
    const { isLoaded, id, loadEvalData } = this.props
    if (!isLoaded) {
      this.timeout = setTimeout(() => {
        loadEvalData.trigger({ id })
      }, 5000);
    }
  }

  render() {
    const { id, isLoaded, error, evaluation } = this.props
    const { reloadStr } = this.state
    if (!isLoaded || error) {
      return (<button onClick={() => { this.props.loadEvalData.trigger({ id });
      this.setState({ isLoaded: !isLoaded })}}>Reload</button>)
    }
    return (
      <div className='result-data'>
        {evaluation.images.map((image, idx) => {
          return (<img key={image+idx} src={v1(`/evaluate/download/${id}_${image}?reloadStr=${reloadStr}`)} />)
        })}
        <button className='reload' onClick={() => {
          this.props.loadEvalData.trigger({ id })
          this.setState({ reloadStr: "" + (new Date()).getSeconds() })
        }}>Reload</button>
        <a className='download' href={v1(`/evaluate/download/${id}`)} ><button>Download</button></a>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/evaluate/reducers/evalData'))
const withSagas = injectSagas(require('comps/evaluate/sagas/evalData'))

const withConnect = connect(
  (state, props) => {
    const { id } = props
    const evaluation = selectEvalDataById(state, id)
    return {
      id,
      evaluation,
      isLoaded: !!evaluation,
    }
  },
  (dispatch) => bindRoutineCreators({ loadEvalData }, dispatch),
)

export default compose(
  withReducer,
  withSagas,
  withConnect,
)(ResultData)
